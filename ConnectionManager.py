import random
import shutil
import socket

import requests
import stem.process
from fake_useragent import UserAgent
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# TODO: replace prints with logger

class ConnectionManager:
    def __init__(self, requests_per_identity=30):
        self.session = None
        self.headers = None
        self.tor_process = None
        self.socks_port = self._gen_port()
        self.tor_dir = '/tmp/tor_' + str(self.socks_port)
        # TODO: remove static path definition

        self.retries = Retry(total=3, status_forcelist=[502, 503, 504])
        self.proxies = {'http:': 'socks5://127.0.0.1:' + str(self.socks_port),
                        'https:': 'socks5://127.0.0.1:' + str(self.socks_port)
                        }

        self.requests_done = 0
        self.requests_per_identity = requests_per_identity

        # os.mkdir(self.tor_dir)

        while self._port_in_use(self.socks_port):
            self.socks_port = self._gen_port()
            # print(f'Generated port: {self.socks_port}')

        self._start_tor()
        self._get_session()

    def request(self, url):
        try:
            if self.requests_done >= self.requests_per_identity:
                self.get_new_connection()

            request = self.session.get(url, timeout=15)

            self.requests_done += 1

            return request
        except requests.exceptions.Timeout as e:
            return f"Connection timeout {e}"
        except requests.exceptions.ConnectionError as e:
            return f"Connection error: {e}"

    def get_new_connection(self):
        self.stop_tor()
        self._start_tor()
        self.requests_done = 0

    def _get_session(self):

        self.session = requests.Session()
        self.headers = {'Accept-Language': 'en-US,en;q=0.8',
                        'Cache-Control': 'max-age=0',
                        'Connection': 'keep-alive',
                        'User-Agent': UserAgent().random,
                        'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/    apng,"
                                  "*/*;q=0.8,application/signed-exchange;v=b3;q=0.9 "
                        }
        self.session.headers = self.headers
        self.session.mount('http://', HTTPAdapter(max_retries=self.retries))
        self.session.proxies = self.proxies

    def _start_tor(self):
        self.tor_process = stem.process.launch_tor_with_config(
            config={'SocksPort': str(self.socks_port),
                    'ExitNodes': '{ru}',
                    'DataDirectory': self.tor_dir,
                    'Log': ['NOTICE file tor.log', 'ERR file tor.log']
                    },
            take_ownership=True,
            timeout=90
        )

    def stop_tor(self):
        self.tor_process.kill()
        shutil.rmtree('/tmp/tor_' + str(self.socks_port), ignore_errors=True)
        # TODO: remove static path definition

    def get_url(self, url):
        while True:
            response = self.request(url)
            if response == 'Connection timeout' or response == 'Connection error':
                self.get_new_connection()
            else:
                return response

    @staticmethod
    def _port_in_use(port):
        # print('Checking if port in use...')
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            return s.connect_ex(('localhost', port)) == 0

    @staticmethod
    def _gen_port():
        return 7000 + random.randint(0, 2000)
