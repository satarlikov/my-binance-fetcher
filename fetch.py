import calendar
import concurrent.futures
import sys
from datetime import datetime, timedelta
from pathlib import Path

import pandas as pd
from loguru import logger

from ConnectionManager import ConnectionManager

DEBUG = False
MAX_WORKERS = 5

logger.add("debug.log", format="{time} {level} {message}", level="DEBUG", rotation="5120 KB", compression="zip")

logger.info("Starting trades collector...")


def get_unix_ms_from_date(date):
    return int(calendar.timegm(date.timetuple()) * 1000 + date.microsecond / 1000)


def get_first_trade_id_from_start_date(sym, f_date, cm):
    # logger.info(f"Getting first trade ID from date {from_date}...")
    new_end_date = f_date + timedelta(seconds=60)
    starttime = get_unix_ms_from_date(f_date)
    endtime = get_unix_ms_from_date(new_end_date)
    # logger.debug(f'https://api.binance.com/api/v3/aggTrades?symbol={symbol}&startTime={startTime}&endTime={endTime}')
    r = cm.get_url(f'https://api.binance.com/api/v3/aggTrades?symbol={sym}&startTime={starttime}&endTime={endtime}')

    count = 3
    while r.status_code != 200:
        logger.warning(f'Get first trade get error! {r.status_code} at {f_date}... {count} tries left')
        cm.get_new_connection()
        count -= 1
        r = cm.get_url(f'https://api.binance.com/api/v3/aggTrades?symbol={sym}&startTime={starttime}&endTime={endtime}')
        if count == 0:
            cm.stop_tor()
            raise Exception('Wrong response')

    response = r.json()
    # logger.debug(response)

    if response == '[]':
        logger.info(f'No trades found at {f_date}')
        cm.stop_tor()
        raise Exception('no trades found')

    if len(response) > 0:
        return response[0]['a']
    else:
        logger.info(f'No trades found at {f_date}')
        cm.stop_tor()
        raise Exception('no trades found')


def get_trades(sym, from_id, cm, f_date):
    r = cm.get_url(f'https://api.binance.com/api/v3/aggTrades?symbol={sym}&limit=1000&fromId={from_id}')

    count = 3  # попыток забрать трейды
    while r.status_code != 200:
        logger.warning(f'Get trades error! {r.status_code} at {f_date}... {count} tries left')
        logger.warning(f'Restarting TOR...{f_date}')
        cm.get_new_connection()
        count -= 1
        r = cm.get_url(f'https://api.binance.com/api/v3/aggTrades?symbol={sym}&limit=1000&fromId={from_id}')
        if count == 0:
            logger.warning(f'Get trades error! {r.status_code} at {f_date}... {count} tries left')
            cm.stop_tor()
            raise Exception(f'Aborting thread... {f_date}')

    return r.json()


def trim(df, t_date):
    return df[df['T'] <= get_unix_ms_from_date(t_date)]


def fetch_binance_trades(sym, f_date):
    logger.info(f'Starting thread {f_date}...')
    cm = ConnectionManager()
    from_id = get_first_trade_id_from_start_date(sym, f_date, cm)
    t_date = f_date + timedelta(days=1) - timedelta(microseconds=1)
    current_time = 0
    df = pd.DataFrame()

    while current_time < get_unix_ms_from_date(t_date):
        trades = get_trades(sym, from_id, cm, f_date)
        current_time = trades[-1]['T']
        from_id = trades[-1]['a']

        # if DEBUG:
        # logger.info(f'fetched {len(trades)} trades /
        # from id {from_id} @ {datetime.utcfromtimestamp(current_time/1000.0)}')

        df = pd.concat([df, pd.DataFrame(trades)])
        # time.sleep(0.1)

    df.drop_duplicates(subset='a', inplace=True)
    df = trim(df, t_date)

    filename = f'binance__{sym}__trades__from__{f_date.strftime("%Y_%m_%d")}.csv'
    df.to_csv(filename)
    cm.stop_tor()
    logger.info(f'{filename} file created!')


def start_fetch(sym, date):
    try:
        fetch_binance_trades(sym, date)
        return True
    except Exception as e:
        logger.warning(f'Thread at {date} stopped ubnormally! {e}')
        return False


def get_missed(sym, f_date, t_date):
    cur_date = f_date
    while cur_date <= t_date:
        filename = f'binance__{sym}__trades__from__{cur_date.strftime("%Y_%m_%d")}.csv'
        csv_file = Path(filename)
        if not csv_file.exists():
            return True

        cur_date = cur_date + timedelta(days=1)

    return False


def get_date(f_date, t_date):
    cur_date = f_date
    while cur_date <= t_date:
        yield cur_date
        cur_date = cur_date + timedelta(days=1)


@logger.catch
def main(sym, f_date, t_date):
    futures = []
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS)

    for date in get_date(f_date, t_date):
        filename = f'binance__{sym}__trades__from__{date.strftime("%Y_%m_%d")}.csv'
        csv_file = Path(filename)
        if not csv_file.exists():
            future = executor.submit(start_fetch, sym, date)
            futures.append(future)

    for future in concurrent.futures.as_completed(futures):
        try:
            result = future.result()
        except:
            logger.warning(f'Thread ended')

    executor.shutdown()


if __name__ == "__main__":
    if len(sys.argv) < 4:
        raise Exception('arguments format: <symbol> <start_date> <end_date>')

    symbol = sys.argv[1]
    from_date = datetime.strptime(sys.argv[2], '%m/%d/%Y')
    to_date = datetime.strptime(sys.argv[3], '%m/%d/%Y') + timedelta(days=1) - timedelta(microseconds=1)
    while get_missed(symbol, from_date, to_date):
        main(symbol, from_date, to_date)
